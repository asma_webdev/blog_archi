<?php
/*
./noyau/classes/ControleurGenerique.php
 */

namespace Noyau\Classes;

abstract class ControleurGenerique {

  protected $_table;
  protected $_gestionnaire;
  public  function __construct(){
    $gestionnaireName = '\App\Modeles\\'
                        .ucfirst($this->_table)
                        .'Gestionnaire';
    $this->_gestionnaire = new $gestionnaireName();
  }
  public function indexAction(array $params = [], string $vue ='index'){
    ${$this->_table} = $this->_gestionnaire->findAll($params);
    include '../app/vues/'.$this->_table.'/'.$vue.'.php';
  }

  public function showAction(array $params = [],  string $vue ='show'){
    ${rtrim($this->_table, 's')} = $this->_gestionnaire->findOneBy($params);
    include '../app/vues/'.$this->_table.'/'.$vue.'.php';
  }

 }
