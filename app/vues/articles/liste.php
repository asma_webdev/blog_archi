<?php
/*
./app/vues/articles/liste.php
Variables disponibles :
    - $articles ARRAY(article)
 */
 use \Noyau\Classes\Template;
?>

 <?php foreach ($articles as $article): ?>

     <li>
       <a href="articles/<?php echo $article->getId(); ?>/<?php echo $article->getSlug(); ?>">
         <?php echo $article->getTitre(); ?>
       </a>
     </li>

 <?php endforeach; ?>
