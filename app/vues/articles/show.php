<?php
/*
./app/vues/articles/show.php
Variables disponibles :
    - $article article
 */
 use \Noyau\Classes\Template;
 ?>
 <?php Template::startZone('title'); ?>
  <?php echo $article->getTitre(); ?>
 <?php Template::stopZone(); ?>

<?php Template::startZone('content1'); ?>
<article class="">
  <header>
    <h1><?php echo $article->getTitre(); ?></h1>
    <div>Auteur: <?php echo $article->getAuteur(); ?> -
      <time datetime="<?php echo \Noyau\Classes\App::datify($article->getDatePublication(), 'Y-m-d'); ?>">
        <?php echo \Noyau\Classes\App::datify($article->getDatePublication()); ?>
      </time>
    </div>
  </header>
  <div class="">
    <?php echo $article->getTexte(); ?>
  </div>
</article>

<?php Template::stopZone(); ?>
