<?php
/*
./app/vues/articles/index.php
Variables disponibles :
    - $articles ARRAY(article)
 */
use \Noyau\Classes\Template;
?>
<?php
 /* ---------------------------------------------------
      ZONE TITLE
    ---------------------------------------------------
  */ ?>
<?php Template::startZone('title'); ?>
  Liste des articles
<?php Template::stopZone(); ?>

<?php
 /* ---------------------------------------------------
      ZONE CONTENT1
    ---------------------------------------------------
  */ ?>
<?php Template::startZone('content1'); ?>

  <h2>Ceci est la page index des articles</h2>
  <?php include '../app/vues/articles/liste.php'; ?>

<?php Template::stopZone(); ?>
