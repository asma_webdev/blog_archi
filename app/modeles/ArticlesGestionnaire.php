<?php
/*
./app/modeles/articlesGestionnaire.php
 */

namespace App\Modeles;
use \Noyau\Classes\App;

class articlesGestionnaire extends \Noyau\Classes\GestionnaireGenerique {

  public function __construct(){
    $this->_table = 'articles';
    parent::__construct();
  }

public function findAllByCategorie(int $id ){//id de categories
  $sql="SELECT *
        FROM articles
        JOIN articles_has_categories ON article=articles.id
        WHERE categorie = :id;";
  $rs= App::getConnexion()->prepare($sql);
  $rs->bindValue(':id', $id, \PDO::PARAM_INT);
  $rs->execute();
  return $this->convertPDOStatementToArrayObj($rs);
}

public function findAllByAuteur(int $id){//id de articles
  $sql="SELECT *
        FROM articles
        where auteur = (
              SELECT auteur
              FROM  articles
              where articles.id = :id);";
  $rs= App::getConnexion()->prepare($sql);
  $rs->bindValue(':id', $id, \PDO::PARAM_INT);
  $rs->execute();
  return $this->convertPDOStatementToArrayObj($rs);
}

}
