<?php
/*
./app/controleurs/articlesControleur.php
 */

namespace App\Controleurs;

class articlesControleur extends \Noyau\Classes\ControleurGenerique {

  public function __construct(){
    $this->_table = 'articles';
    parent::__construct();
  }

  public function indexByCategorieAction(int $id){
    $articles = $this->_gestionnaire->findAllByCategorie($id);
    include '../app/vues/articles/liste.php';
  }

  public function indexByAuteurAction(int $id){
    $articles = $this->_gestionnaire->findAllByAuteur($id);
    include '../app/vues/articles/listAuteur.php';
  }

}
