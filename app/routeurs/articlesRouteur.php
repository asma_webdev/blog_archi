<?php
/*
  ./app/routeurs/articlesRouteur.php
 */

$ctrl = new \App\Controleurs\articlesControleur();

 switch ($_GET['articles']):
   case 'show':
       $ctrl->showAction([
         'findOneBy' => 'slug',
         'data'      => $_GET['slug']
       ]);

       $ctrl->indexByAuteurAction($_GET['id']);

     break;

   default:
      die("Aucune route des articles ne correspond.");
     break;
 endswitch;
