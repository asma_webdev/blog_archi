<?php

/* -----------------------------------------
    PARAMETRES DE CONNEXION A LA DB
   -----------------------------------------
 */
 define('DBHOST', '127.0.0.1');
 define('DBNAME', 'blog_archi_db');
 define('DBUSER', 'root');
 define('DBPWD' , 'root');

 /*
 define('PUBLIC_PATH', 'public');
 define('ADMIN_PATH' , 'backoffice');
*/

 /* -----------------------------------------
     AUTRES PARAMETRES
    -----------------------------------------
  */
  define('DATE_FORMAT', 'D M, Y');

  $zones = ['title', 'content1', 'content2'];
