<?php
/*
  ./app/routeur.php
 */

 if (isset($_GET['articles'])):
   include '../app/routeurs/articlesRouteur.php';

 elseif (isset($_GET['categories'])):
   include '../app/routeurs/categoriesRouteur.php';

 else:

   /*
   ROUTE 1:
   PATTERN: /
   CTRL: ArticlesControleur
   ACTION: indexAction
    */
    $ctrl = new \App\Controleurs\articlesControleur();
    $ctrl->indexAction();
  endif;
